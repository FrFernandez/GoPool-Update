app.directive('carousel',function(){
    return {
        link: function(scope,el,attr){
            scope.$watch(function(){
                $(el).carousel({
                    pause: 'hover',
                    keyboard: true                
                })
            })
        }
    }
})